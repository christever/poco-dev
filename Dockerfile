FROM ubuntu:latest

RUN apt-get update && apt-get install -y --no-install-recommends \
    autoconf automake libtool curl make g++ unzip \
    build-essential ca-certificates \
    libgtest-dev libpoco-dev libpoco-doc \
    nodejs git unzip wget
  
# install latest cmake
RUN apt remove --purge --auto-remove cmake && \
    mkdir /usr/src/cmake && \
    cd /usr/src/cmake && \
    wget --no-check-certificate https://cmake.org/files/v3.13/cmake-3.13.3.tar.gz && \
    tar -xzvf cmake-3.13.3.tar.gz && \
    cd cmake-3.13.3 && \
    ./bootstrap && \
    make -j4 && \
    make install

# build and install gtest
RUN cd /usr/src/gtest && \
    mkdir build && \
    cd build && \
    cmake .. && make && \
    cp *.a /usr/lib && \
    cp -r /usr/src/gtest/include /usr/local

# build and install gmock
RUN cd /usr/src/googletest/googlemock && \
    mkdir build && \
    cd build && \
    cmake .. && make && \
    cp *.a /usr/lib && \
    cp -r /usr/src/googletest/googlemock/include/gmock /usr/local/include

# build and install protobuf
RUN cd ~/ && \
    wget https://github.com/protocolbuffers/protobuf/releases/download/v3.0.2/protobuf-cpp-3.0.2.tar.gz  && \
    tar xzf ./protobuf-cpp-3.0.2.tar.gz && \
    cd ./protobuf-3.0.2/  && \
    ./configure && \
    make && \
    make install && \
    ldconfig && \
    # cleanup
    cd ~/ && \
    rm -rvf protobuf-3.0.2/ && \
    rm ./protobuf-cpp-3.0.2.tar.gz && \
    # check version
    protoc --version 

RUN groupadd developer && useradd --create-home --no-log-init --gid developer developer

# ENV INVENCO_TOOLCHAIN=/home/developer
# ENV INVENCO_TOOLCHAIN_UPDATE=FALSE
ENV TARGET_PLATFORM="x86"

USER developer
WORKDIR /home/developer

ADD pusher.zip /home/developer
RUN unzip /home/developer/pusher.zip && \
    rm /home/developer/pusher.zip
