var Pusher = require('pusher');

var _cfg = JSON.parse(process.argv[2]),
    _projId = process.argv[3],
    _pipelineId = process.argv[4],
    _token = process.argv[5];

var pusher = new Pusher({
  appId: _cfg.credentials.appId,
  key: _cfg.credentials.key,
  secret: _cfg.credentials.secret,
  cluster: _cfg.credentials.cluster,
  useTLS: true
});

var content = {
  projId: _projId,
  pipelineId: _pipelineId,
  token: _token
};

pusher.trigger(_cfg.message.channel, _cfg.message.event, {
  message: content
});

console.log('message sent');
console.log(content);
